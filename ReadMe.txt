
############## Prerequisites ################

## If you do not have some libraries please install them. I gave what I remember incase if I forgot I 		am sorry.

conda install keras
conda install matplotlib
conda install opencv


############## Contents of ZIP file ###########

This zip file contains:
1) input_images folder (Input images that will be an input for parking_spots.py file).
2) model.py (Model that we can to train).
3) modelCar.h5 (Outfile of already trained model).
4) parking_spots.py (Python file which will detect the empty and number of empty and over all parking 		spots. It will use the modelCar.h5 file).
5) spot_dict.pickle (Map file of all the parking cordinates of that parking spot).
6) ReadMe.txt (This file).
7) train_test_data folder (It consists of train and test(validation) images for the model which are 	categorised into occupied and vacant. CNN model uses this images to train and validate the model).


############## After un-zipping of ZIP file ################
1) If you have any .DS_store file please delete that file.
2) To find whether that file is preset or not use this command after navigating to the root folder of 		this project (ls -a).
3) If you have .DS_Store file then use below command to delete it:
	Command:  find . -name '.DS_Store' -type f -delete

Note: .DS_Store file can be present in the root folder of the project or in the test/train folders of train_test_data folder. Please delete it if it is present anywhere mentioned.

############## To Train Model (model.py) ##############
1) You can train the model once again if you want.
2) Output after training the model is spot_dict.pickle file and modelCar.h5  file.
3) If you train the model you can see the model accuracy and model loss at the end.
3) Run command: python model.py



########## To see the output image with parking spots and total and empty spots on it ############
1) Run command: python parking_spots.py
2) AS soon as you run the command in your terminal then you will see a pop up of input images just to 		show you the input images.
3) Close the pop-up then program will continue after this and it will display one more pop up at the 		last with the final images with vacant parking spot and total number and available parking 			spot on the image.